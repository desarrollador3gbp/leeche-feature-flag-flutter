part of '../feature_flag.dart';

class FeatureFlagCore {
  static final FeatureFlagCore _instance = FeatureFlagCore._internal();
  FeatureFlagCore._internal();
  factory FeatureFlagCore({
    Unleash? unleash,
    bool defaultValue = true,
  }) {
    _defaultValue = defaultValue;
    if(unleash != null) _unleash = unleash;
    return _instance;
  }

  static late Unleash _unleash;
  static Unleash get unleash => _unleash;

  static bool _defaultValue = true;
  static bool get defaultValue => _defaultValue;

}