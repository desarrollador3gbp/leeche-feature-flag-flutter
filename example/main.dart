import 'package:feature_flag/core/feature_flag_interface.dart';

class ExampleFeatureFlag extends FeatureFlagInterface {
  ExampleFeatureFlag(String id) : super(id);

  /// flag: `menu_access_home`
  static ExampleFeatureFlag menuAccessHome = ExampleFeatureFlag("menu_access_home");
}

void main() {
  
}