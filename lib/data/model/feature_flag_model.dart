part of '../../feature_flag.dart';

class FeatureFlagModel extends FeatureFlagInterface {
  FeatureFlagModel(String id, {
    bool preview = false,
    String? message,
  }) : super(
    id,
   //  preview: preview,
   //  message: message,
  );

  factory FeatureFlagModel.cast(FeatureFlagInterface e) {
    return FeatureFlagModel(
      e.id,
      // preview: e.preview,
      // message: e.message,
    );
  }

  FeatureFlagModel decode(Map<String, dynamic> json) => FeatureFlagModel(
      id,
      // preview: json["preview"] ?? false,
      // message: json["message"],
  );
}