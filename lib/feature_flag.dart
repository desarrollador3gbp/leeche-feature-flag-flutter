library feature_flag;

import 'package:flutter/material.dart';
import 'core/feature_flag_interface.dart';
import 'package:unleash/unleash.dart';

export 'package:unleash/unleash.dart';
export 'core/feature_flag_interface.dart';

part 'core/feature_flag_core.dart';
part 'core/widget/feature_flag_widget.dart';
part 'core/widget/multi_feature_flag_widget.dart';
part 'core/extensions/feature_flag_extension.dart';
part 'data/model/feature_flag_model.dart';