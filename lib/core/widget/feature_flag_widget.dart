part of '../../feature_flag.dart';

class FeatureFlagWidget extends StatelessWidget {
  final FeatureFlagInterface? flag;
  final Widget child;
  final Widget? offWidget;
  final Widget? loadingWidget;
  final double opacity;
  const FeatureFlagWidget({
    Key? key,
    @required this.flag,
    required this.child,
    this.offWidget,
    this.loadingWidget,
    this.opacity = 0.5,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _toggleByCondition();
  }

  Widget _toggleByCondition() {
    if(flag == null) return child;
    final _toggle = FeatureFlagCore.unleash.isEnabledToggle(flag!.id, defaultValue: FeatureFlagCore.defaultValue);
    return _toggle.enabled == true
        ? child
        : _toggle.description?.isEmpty == false
          ? Tooltip(
              message: _toggle.description,
              triggerMode: TooltipTriggerMode.tap,
              child: IgnorePointer(
                ignoring: true,
                child: Opacity(
                  opacity: opacity,
                  child: child,
                ),
              ),
            )
          : offWidget ?? const SizedBox();
  }

  static Widget? isEnabled({
    @required FeatureFlagInterface? flag,
    required Widget child,
    Widget? offWidget,
    double opacity = 0.5
  }) {
    if(flag == null) return child;
    final _toggle = FeatureFlagCore.unleash.isEnabledToggle(flag.id, defaultValue: FeatureFlagCore.defaultValue);
    bool _enabled = _toggle.enabled == true;
    if(_enabled) {
      return _toggle.enabled == true
        ? child
        : _toggle.description?.isEmpty == false
          ? Tooltip(
              message: _toggle.description,
              triggerMode: TooltipTriggerMode.tap,
              child: IgnorePointer(
                ignoring: true,
                child: Opacity(
                  opacity: opacity,
                  child: child,
                ),
              ),
            )
          : offWidget ?? const SizedBox();
    }
    return null;
  }
}